# Dotfiles
My configurations for programs.

## Requirements
The Neovim dots (.config/nvim) require Neovim 0.5.0 or greater, with packer installed.\
The system info script & .zshrc requires a nerd font (https://www.nerdfonts.com/).

## Installation
`git clone https://gitdab.com/endie/dotfiles.git`, then move/symlink the files you want to the 
correct places relative to your home directory\
Alternatively, you can clone these directly into your home directory with 
`git clone --separate-git-dir=$HOME/dotfiles https://gitdab.com/endie/dotfiles.git ~`,
but that will not work if the files are already in your home directory

## Help
### None of the Neovim configuration is working / errors when starting Neovim
* Make sure your Neovim meets the requirements
> * Currently 0.5.0 is not released yet, so you will have to use a prerelease build
> * Install packer by running 
`git clone https://github.com/wbthomason/packer.nvim
~/.local/share/nvim/site/pack/packer/start/packer.nvim`
* If your Neovim meets the requirements, try running `:PackerSync` in Neovim

### The Materia GTK theme looks wrong when generated with the oomox colors
* Run `find /path/to/generated_theme -name "*.css" -print | xargs sed -i "s/230000/242424/g"`,
replacing `/path/to/generated_theme` with the location of the theme you generated 
(by default ~/.themes/oomox-Pastls).

## Credits
Method for storing dotfiles:
https://www.anand-iyer.com/blog/2018/a-simpler-way-to-manage-your-dotfiles.html
