augroup formatting
  autocmd!
  autocmd BufWritePre * retab!
  autocmd BufWritePre *.py Black
augroup END
