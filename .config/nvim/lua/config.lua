vim.g.mapleader = "`"
vim.o.termguicolors = true

local window_settings = {
    relativenumber = true,
}

local buffer_settings = {
    tabstop = 4,
    shiftwidth = 4,
    expandtab = true,
}

for setting, value in pairs(window_settings) do
    vim.o[setting] = value
    vim.wo[setting] = value
end

for setting, value in pairs(buffer_settings) do
    vim.o[setting] = value
    vim.bo[setting] = value
end
