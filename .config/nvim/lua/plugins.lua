require("packer").startup(
    {
        {
            -- plugin manager
            "wbthomason/packer.nvim",

            -- colors
            {
                "olimorris/onedark.nvim", 
                requires = "rktjmp/lush.nvim",
            },

            -- syntax parsing
            "sheerun/vim-polyglot",

            -- completion
            "ervandew/supertab",

            -- commenting / uncommenting
            "b3nj5m1n/kommentary",

            -- highlights indent
            {
                "lukas-reineke/indent-blankline.nvim",
                branch = "lua",
            },

            -- underlines all words that match the word under the cursor
            "yamatsum/nvim-cursorline",

            -- helps to find cursor when it jumps a far distance
            {
                "edluffy/specs.nvim",
                config = function()
                    require("specs").setup({
                            show_jumps = true,
                            min_jump = 30,
                            popup = {
                                delay_ms = 0,
                                inc_ms = 10,
                                blend = 10,
                                width = 30,
                                winhl = "PMenu",
                                fader = require("specs").pulse_fader,
                                resizer = require("specs").shrink_resizer,
                            },
                            ignore_filetypes = {},
                            ignore_buftypes = {
                                nofile = true,
                            },
                        })
                end,
            },

            -- highlights color codes with their color
            {
                "norcalli/nvim-colorizer.lua", 
                config = function() 
                    require("colorizer").setup() 
                end,
            },

            -- automatically adds matching brackets, quotation marks, etc
            {
                "windwp/nvim-autopairs",
                config = function()
                    require("nvim-autopairs").setup()
                end,
            },

            -- finds errors & warnings in code
            "vim-syntastic/syntastic",

            -- git integration
            "tpope/vim-fugitive",

            -- tools for rust programming
            "rust-lang/rust.vim",

            -- python formatter
            {
                "psf/black",
                run = ":BlackUpgrade",
            },
        },
    }
    )

-- Setting plugin-specific variables --

-- onedark.nvim
vim.g.onedark_override = {
    -- my personal color scheme, Pastls
    red = "#e6b3b3",
    green = "#bde6b3",
    yellow = "#e6e0b3",
    blue = "#b3c7e6",
    purple = "#ccb3e6",
    cyan = "#b3e6d6",
    white = "#ccc",
    black = "#141414",
    gray = "#bdbdbd",
    comment_gray = "#a3a3a3",
    selection_gray = "#333",
    indentline_gray = "#a3a3a3",
    colorcolumn_gray = "#242424",
}
vim.o.background = "dark"

-- lush.nvim
vim.g.colors_name = "onedark_nvim"

-- syntastic
vim.g.syntastic_always_populate_loc_list = 1
vim.g.syntastic_auto_loc_list = 1
vim.g.syntastic_check_on_open = 1
vim.g.syntastic_check_on_wq = 1

-- rust.vim
vim.g.rustfmt_autosave = 1
